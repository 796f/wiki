# wiki
![](http://4x13.net/icons/wiki.png)

Wiki is a clone of wikiwiki, with added features, written in Python3. 

**Install:** Download the repo, and change the settings where relevant. 

**Requires:** Mistune, the Python Markdown module

**Cool features:** Page revision history, "recent changes" page, backlinking, IP storage, "view all" 

If you create new pages with CamelCase names (no spaces, no special characters), you can link to them just by naming them.
You can otherwise link to pages by wrapping the [[Title]] in double square brackets. Other formatting is markdown based.

Try it out here: http://4x13.net/wiki
